// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"log"

	"github.com/godbus/dbus"
	"github.com/golang/protobuf/jsonpb"

	"gitlab.com/redfield/usbctl/api"
)

func main() {
	m := jsonpb.Marshaler{
		EnumsAsInts:  true,
		EmitDefaults: true,
		Indent:       "\t",
		OrigName:     true,
		AnyResolver:  nil,
	}

	bus, err := dbus.SystemBus()
	if err != nil {
		log.Print(err)
		return
	}

	usbdev := api.UsbDevice{
		Bus:  3,
		Addr: 2,
	}

	r := api.AttachRequest{
		Domid:  "9",
		Device: &usbdev,
	}

	s, err := m.MarshalToString(&r)
	if err != nil {
		log.Print(err)
		return
	}

	var reply string
	busObj := bus.Object("com.gitlab.redfield.api.usbctl", "/com/gitlab/redfield/api/usbctl")
	err = busObj.Call("com.gitlab.redfield.api.api.Attach", 0, s).Store(&reply)

	if err != nil {
		log.Print(err)
	}

	fmt.Println(reply)
}
